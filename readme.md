# User Analytics
## Objectives
- Segment users based on their browsing activity.
- Generate product catalog insights on category/subcategory level. 
-  Identify the major factors affecting users' buying decisions. 

## Installation


```bash
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

## Steps Followed in ML Process
### Segment users based on their browsing activity.

#### Record Info
- No of records 801809
- unique users - 179168
- 5 days tracking data from 2019-11-27 00:10:48 to 2019-12-01 00:02:39
- Main feature affecting user segment - action_type. This feature has 7 different actions done.

```
view                    796899
add_to_cart               4045
remove_from_cart           395
add_to_wishlist            208
place_order                136
register                   110
remove_from_wishlist        17
```
#### Step 1
##### Data understanding
- Checking record counts, identifying columns to be used and feature extraction.

#### Step 2
##### Dealing with unbalanced data
- Resampling unbalanced data using **imbalanced-learn API** [docs](https://imbalanced-learn.readthedocs.io/en/stable/api.html) 
- Creating training and test datasets
```
Trainingtargetstatistics: Counter({
  'view': 40,
  'add_to_cart': 38,
  'add_to_wishlist': 37,
  'register': 36,
  'place_order': 36,
  'remove_from_cart': 35,
  'remove_from_wishlist': 15
})
```
```
Testingtargetstatistics: Counter({
  'remove_from_cart': 15,
  'place_order': 14,
  'register': 14,
  'add_to_wishlist': 13,
  'add_to_cart': 12,
  'view': 10,
  'remove_from_wishlist': 2
})
```

#### Step 2
- Segment users using RFM behaviour based approach
- Calculating frequency, recency, monetory and non monetory features.
1. recency - difference between epoch and next day of max date in epoch (2 dec 2019).
2. frequency - count of number of actions performed in website (all action_type) monetory + non monetory
3. monetory - total number of order placed action type
- Calculating RFM Score by quantile cut points.
- Based on RFM score segmenting user into loyal,potential and hibernator.
```
rfm score > 150 -> Loyal User
rfm score > 50 < 150 -> Potential Loyalist
rfm score > 0 < 50 -> Hibernators
```

![Pie Chart for User Segment]("https://gitlab.com/sunilv/user-analytics-ml/-/blob/8d3022ee62d1b836332e8ccb3be554d03b274325/segment.png")


### Generate product catalog insights on category/subcategory level
- Understanding categories and sub categories available. Added "unknown" to blank category.
```
category
Collections            299
Gifting                  1
Jeweller               159
Jewellery               66
Jewellery Type           5
Mia                    119
Silver                  19
tq-mia-category        639
tq-mia-collections      12
tq-occasion             16
unknown               5727
dtype: int64
```
- Redefining Categories using description. Ex: If ring is found in product discription, new category "Ring" is created.Final category list.
```
category
Chain           206
Collections      35
Designs        1146
Gifting          15
Mia             194
Pendant         996
Ring           3770
unknown         700
dtype: int64
```
- Now Merging Product data and user analytics data to figure out which actions done on which products.
- List of actions choosen are Viewd, purchased and wishlist

```
 	viewed 	purchased 	wishlist
category 			
Chain 	3 	0 	3
Designs 	0 	53 	53
Mia 	0 	0 	0
Pendant 	0 	28 	28
Ring 	0 	0 	0
unknown 	2 	2 	2
```






